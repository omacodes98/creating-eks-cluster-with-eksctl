# Demo Project - EKS cluster with eksctl 

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

Create EKS cluster using ekscrl tool that reduces the manual effort of creating an EKS cluster 

## Technologies Used 

* Kubernetes

* AWS EKS 

* Eksctl 

* Linux 

## Steps 

Step 1: Install eksctl on your local computer 

    brew tap waveworks/tap 

    brew install waveworks/tap/eksctl 

[Installing eksctl 1](/images/01_installing_eksctl.png)
[Installing eksctl 2](/images/02_installing_eksctl_2.png)

Step 2: Configure AWS credentials 

[Conifugred AWS credentials](/images/03_configure_aws_credentials.png)

Step 3: Create EKS cluster with eksctl

    eksctl create cluster \
    --name demo-cluster \
    --version 1.24 \
    --region eu-west-3 \
    --nodegroup-name demo-nodes \
    --node-type t2.micro \
    --node 2 \
    --node-min 1 \
    --node-max 3

[Command to create eks cluster](/images/04_creating_eks_cluster_with_eksctl.png)

[Eksctl creating cluster](/images/04_creating_eks_cluster_with_eksctl_1.png)

[Created EKS cluster](/images/05_created_eks_cluster.png)

Step 4: Go on AWS and check if the cluster was created 

[EKS cluster on aws](/images/06_eks_cluster_on_aws.png)

[EC2 instances created](/images/07_ec2_instances_created.png)


## Installation

    brew install waveworks/tap/eksctl 

## Usage 

    eksctl create cluster \
    --name demo-cluster \
    --version 1.24 \
    --region eu-west-3 \
    --nodegroup-name demo-nodes \
    --node-type t2.micro \
    --node 2 \
    --node-min 1 \
    --node-max 3

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/creating-eks-cluster-with-eksctl.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/creating-eks-cluster-with-eksctl

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.